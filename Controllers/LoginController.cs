﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    [Produces("application/json")]
    [Route("api/Login")]
    public class LoginController : Controller
    {
        private readonly ApiDbContext _context;
        public LoginController(ApiDbContext context)
        {

            _context = context;
            //context.Database.EnsureCreated();
        }

        [HttpPost]
        public ActionResult Login([FromBody]User loginUser)
        {
            if (string.IsNullOrEmpty(loginUser.Username) || string.IsNullOrEmpty(loginUser.Password))
            {
                return BadRequest(false);
            }
            var result = _context.User.Where(u => u.Username.Equals(loginUser.Username) && u.Password.Equals(loginUser.Password)).FirstOrDefault();
            if (result == null) return NotFound(false);
            return Ok(true);
        }
    }
}